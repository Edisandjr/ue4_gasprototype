// Fill out your copyright notice in the Description page of Project Settings.


#include "AttributeSetBase.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "GAS_Prototype/Characters/CharacterBase.h"

UAttributeSetBase::UAttributeSetBase() : Health (200.0f), 
MaxHealth(200.0f), 
Mana(100.0f), 
MaxMana(100.0f), 
Focus(250.0f),
MaxFocus(250.0f)
{

}

void UAttributeSetBase::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	//Health
	if (Data.EvaluatedData.Attribute.GetUProperty() == 
		FindFieldChecked<UProperty>(UAttributeSetBase::StaticClass(),GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Health)))
	{
		Health.SetCurrentValue(FMath::Clamp(Health.GetCurrentValue(), 0.0f, MaxHealth.GetCurrentValue()));
		Health.SetBaseValue(FMath::Clamp(Health.GetBaseValue(), 0.0f, MaxHealth.GetCurrentValue()));
		UE_LOG(LogTemp, Warning, TEXT("Took Damage, health is now: %f"), Health.GetCurrentValue());
		OnHealthChange.Broadcast(Health.GetCurrentValue(), MaxHealth.GetCurrentValue());
		ACharacterBase* CharacterOwner = Cast<ACharacterBase>(GetOwningActor());
		if (Health.GetCurrentValue() == MaxHealth.GetCurrentValue())
		{
			if (CharacterOwner)
			{
				CharacterOwner->AddGameplayTag(CharacterOwner->FullHealthTag);
			}
		}
		else 
		{
			if (CharacterOwner)
			{
				CharacterOwner->RemoveGameplayTag(CharacterOwner->FullHealthTag);
			}
		}
	}
	//Mana
	if (Data.EvaluatedData.Attribute.GetUProperty() ==
		FindFieldChecked<UProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Mana)))
	{
		Mana.SetCurrentValue(FMath::Clamp(Mana.GetCurrentValue(), 0.0f, MaxMana.GetCurrentValue()));
		Mana.SetBaseValue(FMath::Clamp(Mana.GetBaseValue(), 0.0f, MaxMana.GetCurrentValue()));
		UE_LOG(LogTemp, Warning, TEXT("Spell Casted/Mana Drained, Mana is now: %f"), Mana.GetCurrentValue());
		OnManaChange.Broadcast(Mana.GetCurrentValue(), MaxMana.GetCurrentValue());
	}
	//Focus
	if (Data.EvaluatedData.Attribute.GetUProperty() ==
		FindFieldChecked<UProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Focus)))
	{
		Focus.SetCurrentValue(FMath::Clamp(Focus.GetCurrentValue(), 0.0f, MaxFocus.GetCurrentValue()));
		Focus.SetBaseValue(FMath::Clamp(Focus.GetBaseValue(), 0.0f, MaxFocus.GetCurrentValue()));
		UE_LOG(LogTemp, Warning, TEXT("Focus Used/Focus Drained, Focus is now: %f"), Focus.GetCurrentValue());
		OnFocusChange.Broadcast(Focus.GetCurrentValue(), MaxFocus.GetCurrentValue());
	}
}
