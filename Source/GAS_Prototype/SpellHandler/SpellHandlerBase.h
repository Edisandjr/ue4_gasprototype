// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Abilities/GameplayAbility.h"
#include "GAS_Prototype/Spellhandler/SpellBase.h"
#include "SpellHandlerBase.generated.h"
/**
 * 
 */
UCLASS(ClassGroup = (Custom), BlueprintType, Blueprintable, meta = (BlueprintSpawnableComponent))
class GAS_PROTOTYPE_API USpellHandlerBase : public UObject
{
	GENERATED_BODY()
public:
	
};
