// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GAS_Prototype/Spellhandler/SpellHandlerBase.h"
#include "Abilities/GameplayAbility.h"
#include "SpellBase.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), BlueprintType, Blueprintable, meta = (BlueprintSpawnableComponent))
class GAS_PROTOTYPE_API USpellBase : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag Augment;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag Unique;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag Shape;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag CC;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag SpellDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag SpellCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag SpellCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag SpellCue;
};
