// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAS_Prototype.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GAS_Prototype, "GAS_Prototype" );
 