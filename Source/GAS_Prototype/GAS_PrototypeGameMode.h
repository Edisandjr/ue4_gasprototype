// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAS_PrototypeGameMode.generated.h"

UCLASS(minimalapi)
class AGAS_PrototypeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAS_PrototypeGameMode();
};



