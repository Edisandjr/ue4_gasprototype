// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "Abilities/GameplayAbility.h"
#include "CharacterBase.generated.h"


class UAttributeSetBase;
class UGameplayAbilityBase;

UCLASS()
class GAS_PROTOTYPE_API ACharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Character Base")
	UAbilitySystemComponent* AbilitySystemComp;
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Character Base")
		UAttributeSetBase* AttributeSetBaseComp;

	
	UFUNCTION(BlueprintCallable, Category = "Character Base")
	void AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAquire);

	UFUNCTION(BlueprintCallable, Category = "Character Base")
	void AquireAbilities(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAquire);

	UFUNCTION()
		void OnHealthChanged(float Health, float MaxHealth);
	UFUNCTION(BlueprintImplementableEvent, Category = "Character Base", meta = (DisplayName = "OnHealthChanged"))
		void BP_OnHealthChanged(float Health, float MaxHealth);

	UFUNCTION()
		void OnManaChanged(float Mana, float MaxMana);
	UFUNCTION(BlueprintImplementableEvent, Category = "Character Base", meta = (DisplayName = "OnManaChanged"))
		void BP_OnManaChanged(float Mana, float MaxMana);

	UFUNCTION()
		void OnFocusChanged(float Focus, float MaxFocus);
	UFUNCTION(BlueprintImplementableEvent, Category = "Character Base", meta = (DisplayName = "OnFocusChanged"))
		void BP_OnFocusChanged(float Focus, float MaxFocus);

	UFUNCTION(BlueprintImplementableEvent, Category = "Character Base", meta = (DisplayName = "Die"))
		void BP_Die();
	UFUNCTION(BlueprintCallable, Category = "Character Base")
		bool IsOtherHostile(ACharacterBase* Other);
	UFUNCTION(BlueprintCallable, Category = "CategoryBase")
		void AddGameplayTag(FGameplayTag TagToAdd);
	UFUNCTION(BlueprintCallable, Category = "CategoryBase")
		void RemoveGameplayTag(FGameplayTag TagToRemove);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Base")
		FGameplayTag FullHealthTag;
	
	uint8 GetTeamID() const;
	UFUNCTION(BlueprintCallable, Category = "CharacterBase")
		void HitStun(float StunDuration);	
	
protected:
	uint8 TeamID;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Character Base")
	bool bIsDead;

	void AutoDetermineTeamIDByControllerType();
	void DeathSequence();
	void DisableInputControls();
	void EnableInputControls();
	FTimerHandle StunTimeHandle;
	void AddAbilityToUI(TSubclassOf<UGameplayAbilityBase> AbilityToAdd);
};
