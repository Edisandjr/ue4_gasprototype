// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBase.h"
#include "GAS_Prototype/Attributes/AttributeSetBase.h"
#include "GameFramework/PlayerController.h"
#include "Components/CapsuleComponent.h"
#include "AIController.h"
#include "BrainComponent.h"
#include "GAS_Prototype/Controllers/PlayerControllerBase.h"
#include "GAS_Prototype/GameplayAbility/GameplayAbilityBase.h" 
// Sets default values
ACharacterBase::ACharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AbilitySystemComp = CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystemComp");
	AttributeSetBaseComp = CreateDefaultSubobject<UAttributeSetBase>("AttributeSetBaseComp");
	bIsDead = false;
	TeamID = 255;
}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();
	AttributeSetBaseComp->OnHealthChange.AddDynamic(this, &ACharacterBase::OnHealthChanged);
	AttributeSetBaseComp->OnManaChange.AddDynamic(this, &ACharacterBase::OnManaChanged);
	AttributeSetBaseComp->OnFocusChange.AddDynamic(this, &ACharacterBase::OnFocusChanged);
	AutoDetermineTeamIDByControllerType();
	AddGameplayTag(FullHealthTag);
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ACharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComp;
}

void ACharacterBase::AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAquire)
{
	if (AbilitySystemComp)
	{
		if (HasAuthority() && AbilityToAquire)
		{
			FGameplayAbilitySpecDef SpecDef = FGameplayAbilitySpecDef();
			SpecDef.Ability = AbilityToAquire;
			FGameplayAbilitySpec AbilitySpec = FGameplayAbilitySpec(SpecDef, 1);
			AbilitySystemComp->GiveAbility(AbilitySpec);
		}
	}
	AbilitySystemComp->InitAbilityActorInfo(this, this); 
}
void ACharacterBase::AquireAbilities(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAquire)
{
	for (TSubclassOf<UGameplayAbility> AbilityItem : AbilitiesToAquire)
	{
		AquireAbility(AbilityItem);
		if (AbilityItem->IsChildOf(UGameplayAbilityBase::StaticClass()))
		{
			TSubclassOf<UGameplayAbilityBase> AbilityBaseClass = *AbilityItem;
			if (AbilityBaseClass != nullptr)
			{
				AddAbilityToUI(AbilityBaseClass);
			}
		}
	}
}
void ACharacterBase::OnHealthChanged(float Health, float MaxHealth)
{
	BP_OnHealthChanged(Health, MaxHealth);
	if (Health <= 0.0f && !bIsDead)
	{
		bIsDead = true;
		DisableInputControls();
		DeathSequence();
		BP_Die();
		//Can Crash game if no AI Tree is present.
	}
	
}
void ACharacterBase::OnManaChanged(float Mana, float MaxMana)
{
	BP_OnManaChanged(Mana, MaxMana);
}
void ACharacterBase::OnFocusChanged(float Focus, float MaxFocus)
{
	BP_OnFocusChanged(Focus, MaxFocus);
}
bool ACharacterBase::IsOtherHostile(ACharacterBase* Other)
{
	UE_LOG(LogTemp, Warning, TEXT("Team ID of self: %d | Team ID of Target: %d"),GetTeamID(), Other->GetTeamID());
	return GetTeamID() != Other->GetTeamID();
}
void ACharacterBase::AddGameplayTag(FGameplayTag TagToAdd)
{
	GetAbilitySystemComponent()->AddLooseGameplayTag(TagToAdd);
	GetAbilitySystemComponent()->SetTagMapCount(TagToAdd, 1);
}
void ACharacterBase::RemoveGameplayTag(FGameplayTag TagToRemove)
{
	GetAbilitySystemComponent()->RemoveLooseGameplayTag(TagToRemove);
	//GetAbilitySystemComponent()->SetTagMapCount(TagToRemove, 1);
}
uint8 ACharacterBase::GetTeamID() const
{
	return TeamID;
}
void ACharacterBase::HitStun(float StunDuration)
{
	DisableInputControls();
	GetWorldTimerManager().SetTimer(StunTimeHandle, this, &ACharacterBase::EnableInputControls, StunDuration, false);
}
void ACharacterBase::AutoDetermineTeamIDByControllerType()
{
	if (GetController() && GetController()->IsPlayerController())
	{
		UE_LOG(LogTemp, Warning, TEXT("Player Controller Team ID 0"));
		TeamID = 0;
	}
}
void ACharacterBase::DeathSequence()
{
	//APlayerController* PC = Cast<APlayerController>(GetController());
		//if(PC)
	//UE_LOG(LogTemp, Warning, TEXT("Controls disabled"))
	//GetController()->DisableInput(Cast<APlayerController>((GetController())));
	DisableInputControls();
	DetachFromControllerPendingDestroy();
	GetCapsuleComponent()->DestroyComponent();
	
}

void ACharacterBase::DisableInputControls()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	if(PC)
	{
		PC->DisableInput(PC);
	}
	AAIController* AIC = Cast<AAIController>(GetController());
	if (AIC)
	{
		AIC->GetBrainComponent()->StopLogic("Dead");
	}
}

void ACharacterBase::EnableInputControls()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC)
	{
		PC->EnableInput(PC);
	}
	AAIController* AIC = Cast<AAIController>(GetController());
	if (AIC)
	{
		AIC->GetBrainComponent()->RestartLogic();
	}
}

void ACharacterBase::AddAbilityToUI(TSubclassOf<UGameplayAbilityBase> AbilityToAdd)
{
	APlayerControllerBase* PlayerControllerBase = Cast<APlayerControllerBase>(GetController());
	if (PlayerControllerBase)
	{
		UGameplayAbilityBase* AbilityInstance = AbilityToAdd.Get()->GetDefaultObject<UGameplayAbilityBase>();
		if (AbilityInstance)
		{
			FGameplayAbilityInfo AbilityInfo = AbilityInstance->GetAbilityInfo();
			PlayerControllerBase->AddAbilityToUI(AbilityInfo);
		}
	}
}
