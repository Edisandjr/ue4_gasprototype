// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAS_PrototypeGameMode.h"
#include "GAS_PrototypeHUD.h"
#include "GAS_PrototypeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAS_PrototypeGameMode::AGAS_PrototypeGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AGAS_PrototypeHUD::StaticClass();
}
